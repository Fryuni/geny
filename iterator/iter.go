package iterator

import (
	"gitlab.com/Fryuni/geny"
	"gitlab.com/Fryuni/geny/internal/util"
)

func Each[T any](iterator geny.Iterator[T], f func(T) error) error {
	var value T
	var err error

	for value, err = iterator.Next(); err == nil; value, err = iterator.Next() {
		if err = f(value); err != nil {
			return err
		}
	}

	return err
}

type MapIterator[I, O any] struct {
	iterator geny.Iterator[I]
	f        func(I) (O, error)
}

func Map[I, O any](iterator geny.Iterator[I], f func(I) (O, error)) *MapIterator[I, O] {
	return &MapIterator[I, O]{iterator, f}
}

func (m *MapIterator[I, O]) Next() (O, error) {
	input, err := m.iterator.Next()
	if err != nil {
		return util.Zero[O](), err
	}

	return m.f(input)
}
