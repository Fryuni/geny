package geny

import (
	"testing"
)

func TestInMemoryMapStore(t *testing.T) {
	store := make(InMemoryMapStore[string, string])

	got, ok := store.Get("key")
	if got != "" || ok {
		t.Errorf("got %v, %v, want %v, %v", got, ok, "", false)
	}

	store.Put("key", "value")

	got, ok = store.Get("key")
	if got != "value" || !ok {
		t.Errorf("got %v, %v, want %v, %v", got, ok, "value", true)
	}

	store.Delete("key")

	got, ok = store.Get("key")
	if got != "" || ok {
		t.Errorf("got %v, %v, want %v, %v", got, ok, "", false)
	}
}
