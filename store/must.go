package store

import (
	"gitlab.com/Fryuni/geny"
	"gitlab.com/Fryuni/geny/internal/util"
)

type mustStore[K comparable, V any] struct {
	store geny.Store[K, V]
}

// Must converts a Store to a [geny.InfallibleStore].
// When the underlying Store.Get method returns [geny.ErrNoItem], the [geny.InfallibleStore.Get] will return
// the zero value of V and a false `ok`.
// Any other error will cause a panic.
func Must[K comparable, V any](store geny.Store[K, V]) geny.InfallibleStore[K, V] {
	return mustStore[K, V]{store: store}
}

func (i mustStore[K, V]) Get(key K) (V, bool) {
	value, err := i.store.Get(key)
	switch err {
	case nil:
		return value, true
	case geny.ErrNoItem:
		return util.Zero[V](), false
	default:
		panic(err)
	}
}

func (i mustStore[K, V]) Put(key K, value V) {
	if err := i.store.Put(key, value); err != nil {
		panic(err)
	}
}

func (i mustStore[K, V]) Delete(key K) {
	if err := i.store.Delete(key); err != nil {
		panic(err)
	}
}

type notInfallible[K comparable, V any] struct {
	store geny.InfallibleStore[K, V]
}

// NotInfallible converts a [geny.InfallibleStore] into a [geny.Store].
//
// When the underlying [geny.InfallibleStore.Get] method returns false, the [geny.Store.Get] will return
// the zero value of V and a [geny.ErrNoItem].
// No other error is ever returned.
// Panics will _not_ be automatically converted into errors.
func NotInfallible[K comparable, V any](store geny.InfallibleStore[K, V]) geny.Store[K, V] {
	return notInfallible[K, V]{store: store}
}

func (n notInfallible[K, V]) Get(key K) (V, error) {
	value, ok := n.store.Get(key)
	if !ok {
		return util.Zero[V](), geny.ErrNoItem
	}
	return value, nil
}

func (n notInfallible[K, V]) Put(key K, value V) error {
	n.store.Put(key, value)
	return nil
}

func (n notInfallible[K, V]) Delete(key K) error {
	n.store.Delete(key)
	return nil
}
