package store_test

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"

	"gitlab.com/Fryuni/geny/internal/mocks/gitlab.com/Fryuni/mock_geny"
	"gitlab.com/Fryuni/geny/store"
)

func TestUsingContext(t *testing.T) {
	t.Run("Get", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockStore := mock_geny.NewMockContextStore[string, string](ctrl)

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		innerError := errors.New("inner error")

		mockStore.EXPECT().
			Get(ctx, "key").
			Times(1).
			Return("value", innerError)

		wrapped := store.UsingContext[string, string](ctx, mockStore)

		value, err := wrapped.Get("key")

		if value != "value" || err != innerError {
			t.Errorf("Expected value and error to be forwarded, got value '%v' and error '%v'", value, err)
		}
	})

	t.Run("Put", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockStore := mock_geny.NewMockContextStore[string, string](ctrl)

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		innerError := errors.New("inner error")

		mockStore.EXPECT().
			Put(ctx, "key", "value").
			Times(1).
			Return(innerError)

		wrapped := store.UsingContext[string, string](ctx, mockStore)

		err := wrapped.Put("key", "value")

		if err != innerError {
			t.Errorf("Expected error to be forwarded, got error '%v'", err)
		}
	})

	t.Run("Delete", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockStore := mock_geny.NewMockContextStore[string, string](ctrl)

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		innerError := errors.New("inner error")

		mockStore.EXPECT().
			Delete(ctx, "key").
			Times(1).
			Return(innerError)

		wrapped := store.UsingContext[string, string](ctx, mockStore)

		err := wrapped.Delete("key")

		if err != innerError {
			t.Errorf("Expected error to be forwarded, got error '%v'", err)
		}
	})
}

func TestIgnoringContext(t *testing.T) {
	t.Run("Get", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockStore := mock_geny.NewMockStore[string, string](ctrl)

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		innerError := errors.New("inner error")

		mockStore.EXPECT().
			Get("key").
			Times(1).
			Return("value", innerError)

		wrapped := store.IgnoringContext[string, string](mockStore)

		value, err := wrapped.Get(ctx, "key")

		if value != "value" || err != innerError {
			t.Errorf("Expected value and error to be forwarded, got value '%v' and error '%v'", value, err)
		}
	})

	t.Run("Put", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockStore := mock_geny.NewMockStore[string, string](ctrl)

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		innerError := errors.New("inner error")

		mockStore.EXPECT().
			Put("key", "value").
			Times(1).
			Return(innerError)

		wrapped := store.IgnoringContext[string, string](mockStore)

		err := wrapped.Put(ctx, "key", "value")

		if err != innerError {
			t.Errorf("Expected error to be forwarded, got error '%v'", err)
		}
	})

	t.Run("Delete", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockStore := mock_geny.NewMockStore[string, string](ctrl)

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		innerError := errors.New("inner error")

		mockStore.EXPECT().
			Delete("key").
			Times(1).
			Return(innerError)

		wrapped := store.IgnoringContext[string, string](mockStore)

		err := wrapped.Delete(ctx, "key")

		if err != innerError {
			t.Errorf("Expected error to be forwarded, got error '%v'", err)
		}
	})
}
