package store

import (
	"bytes"
	"context"
	"encoding/gob"
	"encoding/json"

	. "gitlab.com/Fryuni/geny"
	"gitlab.com/Fryuni/geny/internal/util"
)

type transformerStore[IK, IV, OK, OV any] struct {
	store        ContextStore[IK, IV]
	keyEncoder   TransformPair[IK, OK]
	valueEncoder TransformPair[IV, OV]
}

// TransformPair is a pair of transformer functions to convert between an inner and an outer type.
type TransformPair[I, O any] struct {
	TransformIn  func(O) (I, error)
	TransformOut func(I) (O, error)
}

// NewTransformedContextStore converts the type of the key and value of the given [ContextStore]
// using the given [TransformPair]s.
func NewTransformedContextStore[IK, IV, OK, OV any](
	store ContextStore[IK, IV],
	keyEncoder TransformPair[IK, OK],
	valueEncoder TransformPair[IV, OV],
) ContextStore[OK, OV] {
	return &transformerStore[IK, IV, OK, OV]{
		store:        store,
		keyEncoder:   keyEncoder,
		valueEncoder: valueEncoder,
	}
}

// NewTransformedStore converts the type of the key and value of the given [Store]
// using the given [TransformPair]s.
func NewTransformedStore[IK, IV, OK, OV any](
	store Store[IK, IV],
	keyEncoder TransformPair[IK, OK],
	valueEncoder TransformPair[IV, OV],
) Store[OK, OV] {
	return UsingContext(
		context.Background(),
		NewTransformedContextStore[IK, IV, OK, OV](
			IgnoringContext[IK, IV](store),
			keyEncoder,
			valueEncoder,
		),
	)
}

func (e *transformerStore[IK, IV, OK, OV]) Get(ctx context.Context, key OK) (value OV, err error) {
	innerKey, err := e.keyEncoder.TransformIn(key)
	if err != nil {
		return util.Zero[OV](), err
	}

	innerValue, err := e.store.Get(ctx, innerKey)
	if err != nil {
		return util.Zero[OV](), err
	}

	return e.valueEncoder.TransformOut(innerValue)
}

func (e *transformerStore[IK, IV, OK, OV]) Put(ctx context.Context, key OK, value OV) error {
	innerKey, err := e.keyEncoder.TransformIn(key)
	if err != nil {
		return err
	}

	innerValue, err := e.valueEncoder.TransformIn(value)
	if err != nil {
		return err
	}

	return e.store.Put(ctx, innerKey, innerValue)
}

func (e *transformerStore[IK, IV, OK, OV]) Delete(ctx context.Context, key OK) error {
	innerKey, err := e.keyEncoder.TransformIn(key)
	if err != nil {
		return err
	}

	return e.store.Delete(ctx, innerKey)
}

func NewNoTransformPair[T any]() TransformPair[T, T] {
	return TransformPair[T, T]{
		TransformIn:  func(t T) (T, error) { return t, nil },
		TransformOut: func(t T) (T, error) { return t, nil },
	}
}

func NewJsonTransformPair[T any]() TransformPair[[]byte, T] {
	return TransformPair[[]byte, T]{
		TransformIn: func(v T) ([]byte, error) {
			return json.Marshal(v)
		},
		TransformOut: func(data []byte) (value T, err error) {
			err = json.Unmarshal(data, &value)
			return
		},
	}
}

func NewGobTransformPair[T any]() TransformPair[[]byte, T] {
	return TransformPair[[]byte, T]{
		TransformIn: func(v T) ([]byte, error) {
			buf := new(bytes.Buffer)
			err := gob.NewEncoder(buf).Encode(v)
			return buf.Bytes(), err
		},
		TransformOut: func(data []byte) (value T, err error) {
			err = gob.NewDecoder(bytes.NewReader(data)).Decode(&value)
			return
		},
	}
}
