package store

import (
	"context"

	"gitlab.com/Fryuni/geny"
)

type contextStore[K, V any] struct {
	ctx   context.Context
	store geny.ContextStore[K, V]
}

// UsingContext converts a geny.ContextStore to a geny.Store by executing all its methods with the given context.
func UsingContext[K, V any](ctx context.Context, store geny.ContextStore[K, V]) geny.Store[K, V] {
	return &contextStore[K, V]{ctx: ctx, store: store}
}

func (b *contextStore[K, V]) Get(key K) (V, error) {
	return b.store.Get(b.ctx, key)
}

func (b *contextStore[K, V]) Put(key K, value V) error {
	return b.store.Put(b.ctx, key, value)
}

func (b *contextStore[K, V]) Delete(key K) error {
	return b.store.Delete(b.ctx, key)
}

type noContextStore[K, V any] struct {
	store geny.Store[K, V]
}

// IgnoringContext converts a geny.Store to a geny.ContextStore ignoring any context given to the methods.
func IgnoringContext[K, V any](store geny.Store[K, V]) geny.ContextStore[K, V] {
	return &noContextStore[K, V]{store: store}
}

func (n noContextStore[K, V]) Get(_ context.Context, key K) (V, error) {
	return n.store.Get(key)
}

func (n noContextStore[K, V]) Put(_ context.Context, key K, value V) error {
	return n.store.Put(key, value)
}

func (n noContextStore[K, V]) Delete(_ context.Context, key K) error {
	return n.store.Delete(key)
}
