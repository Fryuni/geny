package store_test

import (
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/Fryuni/geny"
	"gitlab.com/Fryuni/geny/internal/mocks/gitlab.com/Fryuni/mock_geny"
	"gitlab.com/Fryuni/geny/store"
)

func TestMust(t *testing.T) {
	t.Run("Get", func(t *testing.T) {
		t.Run("returns the inner value and true when the key is found", func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockStore := mock_geny.NewMockStore[string, string](ctrl)

			mockStore.EXPECT().
				Get("key").
				Times(1).
				Return("value", nil)

			wrapped := store.Must[string, string](mockStore)

			value, ok := wrapped.Get("key")

			assert.Truef(t, ok, "should be true when the key is found")
			assert.Equalf(t, "value", value, "inner value should be forwarded")
		})
		t.Run("returns the zero value and false when the key is not found", func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockStore := mock_geny.NewMockStore[string, string](ctrl)

			mockStore.EXPECT().
				Get("key").
				Times(1).
				Return("value", geny.ErrNoItem)

			wrapped := store.Must[string, string](mockStore)

			value, ok := wrapped.Get("key")

			assert.Falsef(t, ok, "should be false when the key is not found")
			assert.Equalf(t, "", value, "inner value should be ignored when the key is not found")
		})
		t.Run("panic when there is an error", func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockStore := mock_geny.NewMockStore[string, string](ctrl)

			innerError := errors.New("inner error")

			mockStore.EXPECT().
				Get("key").
				Times(1).
				Return("value", innerError)

			wrapped := store.Must[string, string](mockStore)

			assert.Panicsf(t, func() { wrapped.Get("key") }, "should panic when there is an error")
		})
	})
	t.Run("Put", func(t *testing.T) {
		t.Run("no error", func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockStore := mock_geny.NewMockStore[string, string](ctrl)

			mockStore.EXPECT().
				Put("key", "value").
				Times(1).
				Return(nil)

			wrapped := store.Must[string, string](mockStore)

			wrapped.Put("key", "value")
		})
		t.Run("panic on error", func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockStore := mock_geny.NewMockStore[string, string](ctrl)

			innerError := errors.New("inner error")

			mockStore.EXPECT().
				Put("key", "value").
				Times(1).
				Return(innerError)

			wrapped := store.Must[string, string](mockStore)

			assert.Panicsf(t, func() { wrapped.Put("key", "value") }, "should panic on error")
		})
	})
	t.Run("Delete", func(t *testing.T) {
		t.Run("no error", func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockStore := mock_geny.NewMockStore[string, string](ctrl)

			mockStore.EXPECT().
				Delete("key").
				Times(1).
				Return(nil)

			wrapped := store.Must[string, string](mockStore)

			wrapped.Delete("key")
		})
		t.Run("panic on error", func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockStore := mock_geny.NewMockStore[string, string](ctrl)

			innerError := errors.New("inner error")

			mockStore.EXPECT().
				Delete("key").
				Times(1).
				Return(innerError)

			wrapped := store.Must[string, string](mockStore)

			assert.Panicsf(t, func() { wrapped.Delete("key") }, "should panic on error")
		})
	})
}

func TestNotInfallible(t *testing.T) {
	t.Run("Get", func(t *testing.T) {
		t.Run("returns nil when the key is found", func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockStore := mock_geny.NewMockInfallibleStore[string, string](ctrl)

			mockStore.EXPECT().
				Get("key").
				Times(1).
				Return("value", true)

			wrapped := store.NotInfallible[string, string](mockStore)

			value, err := wrapped.Get("key")

			assert.NoErrorf(t, err, "no error should be returned when the key is found")
			assert.Equalf(t, "value", value, "inner value should be forwarded")
		})
		t.Run("returns ErrNoItem when the key is not found", func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockStore := mock_geny.NewMockInfallibleStore[string, string](ctrl)

			mockStore.EXPECT().
				Get("key").
				Times(1).
				Return("value", false)

			wrapped := store.NotInfallible[string, string](mockStore)

			value, err := wrapped.Get("key")

			assert.ErrorIs(t, err, geny.ErrNoItem, "ErrNoItem should be returned when the key is not found")
			assert.Equalf(t, "", value, "inner value should be ignored when the key is not found")
		})
	})
	t.Run("Put", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockStore := mock_geny.NewMockInfallibleStore[string, string](ctrl)

		mockStore.EXPECT().
			Put("key", "value").
			Times(1)

		wrapped := store.NotInfallible[string, string](mockStore)

		err := wrapped.Put("key", "value")

		assert.NoErrorf(t, err, "no error should be returned on this method")
	})
	t.Run("Delete", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockStore := mock_geny.NewMockInfallibleStore[string, string](ctrl)

		mockStore.EXPECT().
			Delete("key").
			Times(1)

		wrapped := store.NotInfallible[string, string](mockStore)

		err := wrapped.Delete("key")

		assert.NoErrorf(t, err, "no error should be returned on this method")
	})
}
