package geny

import (
	"context"
	"io"
)

// ErrIteratorDone is returned by [geny.Iterator.Next] when the iterator will not yield more items.
//
// The value of ErrIteratorDone is the same as the value of [io.EOF] for compatibility with libraries
// that use [io.EOF] to identify when a sequence of items has been exhausted successfully.
var ErrIteratorDone = io.EOF

type (
	// Iterator over a sequence of items.
	Iterator[V any] interface {
		// Next returns the next item and advances the iterator.
		// If the iterator is exhausted, Next returns the zero value of V and ErrIteratorDone.
		Next() (value V, err error)
	}

	// Iterator over a sequence of items.
	ContextIterator[V any] interface {
		// Next returns the next item and advances the iterator.
		// If the iterator is exhausted, Next returns the zero value of V and ErrIteratorDone.
		Next(ctx context.Context) (value V, err error)
	}
)
