// Code generated by `ensure mocks generate`. DO NOT EDIT.
// Source: gitlab.com/Fryuni/geny (interfaces: Store, ContextStore, InfallibleStore, Iterator, ContextIterator)

// Package mock_geny is a generated GoMock package.
package mock_geny

import (
	"context"
	"github.com/golang/mock/gomock"
	"reflect"
)

// MockStore is a mock of the Store interface in gitlab.com/Fryuni/geny.
type MockStore[K any, V any] struct {
	ctrl     *gomock.Controller
	recorder *MockStoreMockRecorder[K, V]
}

// MockStoreMockRecorder is the mock recorder for MockStore.
type MockStoreMockRecorder[K any, V any] struct {
	mock *MockStore[K, V]
}

// NewMockStore creates a new mock instance.
func NewMockStore[K any, V any](ctrl *gomock.Controller) *MockStore[K, V] {
	mock := &MockStore[K, V]{ctrl: ctrl}
	mock.recorder = &MockStoreMockRecorder[K, V]{mock}
	return mock
}

// NEW creates a MockStore. This method is used internally by ensure.
func (*MockStore[K, V]) NEW(ctrl *gomock.Controller) *MockStore[K, V] {
	return NewMockStore[K, V](ctrl)
}

// EXPECT returns a struct that allows setting up expectations.
func (m *MockStore[K, V]) EXPECT() *MockStoreMockRecorder[K, V] {
	return m.recorder
}

// Delete mocks Delete on Store.
func (m *MockStore[K, V]) Delete(_key K) error {
	m.ctrl.T.Helper()
	inputs := []interface{}{_key}
	ret := m.ctrl.Call(m, "Delete", inputs...)
	ret0, _ := ret[0].(error)
	return ret0
}

// Delete sets up expectations for calls to Delete.
// Calling this method multiple times allows expecting multiple calls to Delete with a variety of parameters.
//
// Inputs:
//
//	key K
//
// Outputs:
//
//	error
func (mr *MockStoreMockRecorder[K, V]) Delete(_key interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	inputs := []interface{}{_key}
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Delete", reflect.TypeOf((*MockStore[K, V])(nil).Delete), inputs...)
}

// Get mocks Get on Store.
func (m *MockStore[K, V]) Get(_key K) (_value V, _err error) {
	m.ctrl.T.Helper()
	inputs := []interface{}{_key}
	ret := m.ctrl.Call(m, "Get", inputs...)
	ret0, _ := ret[0].(V)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Get sets up expectations for calls to Get.
// Calling this method multiple times allows expecting multiple calls to Get with a variety of parameters.
//
// Inputs:
//
//	key K
//
// Outputs:
//
//	value V
//	err error
func (mr *MockStoreMockRecorder[K, V]) Get(_key interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	inputs := []interface{}{_key}
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Get", reflect.TypeOf((*MockStore[K, V])(nil).Get), inputs...)
}

// Put mocks Put on Store.
func (m *MockStore[K, V]) Put(_key K, _value V) error {
	m.ctrl.T.Helper()
	inputs := []interface{}{_key, _value}
	ret := m.ctrl.Call(m, "Put", inputs...)
	ret0, _ := ret[0].(error)
	return ret0
}

// Put sets up expectations for calls to Put.
// Calling this method multiple times allows expecting multiple calls to Put with a variety of parameters.
//
// Inputs:
//
//	key K
//	value V
//
// Outputs:
//
//	error
func (mr *MockStoreMockRecorder[K, V]) Put(_key interface{}, _value interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	inputs := []interface{}{_key, _value}
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Put", reflect.TypeOf((*MockStore[K, V])(nil).Put), inputs...)
}

// MockContextStore is a mock of the ContextStore interface in gitlab.com/Fryuni/geny.
type MockContextStore[K any, V any] struct {
	ctrl     *gomock.Controller
	recorder *MockContextStoreMockRecorder[K, V]
}

// MockContextStoreMockRecorder is the mock recorder for MockContextStore.
type MockContextStoreMockRecorder[K any, V any] struct {
	mock *MockContextStore[K, V]
}

// NewMockContextStore creates a new mock instance.
func NewMockContextStore[K any, V any](ctrl *gomock.Controller) *MockContextStore[K, V] {
	mock := &MockContextStore[K, V]{ctrl: ctrl}
	mock.recorder = &MockContextStoreMockRecorder[K, V]{mock}
	return mock
}

// NEW creates a MockContextStore. This method is used internally by ensure.
func (*MockContextStore[K, V]) NEW(ctrl *gomock.Controller) *MockContextStore[K, V] {
	return NewMockContextStore[K, V](ctrl)
}

// EXPECT returns a struct that allows setting up expectations.
func (m *MockContextStore[K, V]) EXPECT() *MockContextStoreMockRecorder[K, V] {
	return m.recorder
}

// Delete mocks Delete on ContextStore.
func (m *MockContextStore[K, V]) Delete(_ctx context.Context, _key K) error {
	m.ctrl.T.Helper()
	inputs := []interface{}{_ctx, _key}
	ret := m.ctrl.Call(m, "Delete", inputs...)
	ret0, _ := ret[0].(error)
	return ret0
}

// Delete sets up expectations for calls to Delete.
// Calling this method multiple times allows expecting multiple calls to Delete with a variety of parameters.
//
// Inputs:
//
//	ctx context.Context
//	key K
//
// Outputs:
//
//	error
func (mr *MockContextStoreMockRecorder[K, V]) Delete(_ctx interface{}, _key interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	inputs := []interface{}{_ctx, _key}
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Delete", reflect.TypeOf((*MockContextStore[K, V])(nil).Delete), inputs...)
}

// Get mocks Get on ContextStore.
func (m *MockContextStore[K, V]) Get(_ctx context.Context, _key K) (_value V, _err error) {
	m.ctrl.T.Helper()
	inputs := []interface{}{_ctx, _key}
	ret := m.ctrl.Call(m, "Get", inputs...)
	ret0, _ := ret[0].(V)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Get sets up expectations for calls to Get.
// Calling this method multiple times allows expecting multiple calls to Get with a variety of parameters.
//
// Inputs:
//
//	ctx context.Context
//	key K
//
// Outputs:
//
//	value V
//	err error
func (mr *MockContextStoreMockRecorder[K, V]) Get(_ctx interface{}, _key interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	inputs := []interface{}{_ctx, _key}
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Get", reflect.TypeOf((*MockContextStore[K, V])(nil).Get), inputs...)
}

// Put mocks Put on ContextStore.
func (m *MockContextStore[K, V]) Put(_ctx context.Context, _key K, _value V) error {
	m.ctrl.T.Helper()
	inputs := []interface{}{_ctx, _key, _value}
	ret := m.ctrl.Call(m, "Put", inputs...)
	ret0, _ := ret[0].(error)
	return ret0
}

// Put sets up expectations for calls to Put.
// Calling this method multiple times allows expecting multiple calls to Put with a variety of parameters.
//
// Inputs:
//
//	ctx context.Context
//	key K
//	value V
//
// Outputs:
//
//	error
func (mr *MockContextStoreMockRecorder[K, V]) Put(_ctx interface{}, _key interface{}, _value interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	inputs := []interface{}{_ctx, _key, _value}
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Put", reflect.TypeOf((*MockContextStore[K, V])(nil).Put), inputs...)
}

// MockInfallibleStore is a mock of the InfallibleStore interface in gitlab.com/Fryuni/geny.
type MockInfallibleStore[K any, V any] struct {
	ctrl     *gomock.Controller
	recorder *MockInfallibleStoreMockRecorder[K, V]
}

// MockInfallibleStoreMockRecorder is the mock recorder for MockInfallibleStore.
type MockInfallibleStoreMockRecorder[K any, V any] struct {
	mock *MockInfallibleStore[K, V]
}

// NewMockInfallibleStore creates a new mock instance.
func NewMockInfallibleStore[K any, V any](ctrl *gomock.Controller) *MockInfallibleStore[K, V] {
	mock := &MockInfallibleStore[K, V]{ctrl: ctrl}
	mock.recorder = &MockInfallibleStoreMockRecorder[K, V]{mock}
	return mock
}

// NEW creates a MockInfallibleStore. This method is used internally by ensure.
func (*MockInfallibleStore[K, V]) NEW(ctrl *gomock.Controller) *MockInfallibleStore[K, V] {
	return NewMockInfallibleStore[K, V](ctrl)
}

// EXPECT returns a struct that allows setting up expectations.
func (m *MockInfallibleStore[K, V]) EXPECT() *MockInfallibleStoreMockRecorder[K, V] {
	return m.recorder
}

// Delete mocks Delete on InfallibleStore.
func (m *MockInfallibleStore[K, V]) Delete(_key K) {
	m.ctrl.T.Helper()
	inputs := []interface{}{_key}
	ret := m.ctrl.Call(m, "Delete", inputs...)
	var _ = ret // Unused, since there are no returns
	return
}

// Delete sets up expectations for calls to Delete.
// Calling this method multiple times allows expecting multiple calls to Delete with a variety of parameters.
//
// Inputs:
//
//	key K
//
// Outputs:
//
//	none
func (mr *MockInfallibleStoreMockRecorder[K, V]) Delete(_key interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	inputs := []interface{}{_key}
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Delete", reflect.TypeOf((*MockInfallibleStore[K, V])(nil).Delete), inputs...)
}

// Get mocks Get on InfallibleStore.
func (m *MockInfallibleStore[K, V]) Get(_key K) (_value V, _ok bool) {
	m.ctrl.T.Helper()
	inputs := []interface{}{_key}
	ret := m.ctrl.Call(m, "Get", inputs...)
	ret0, _ := ret[0].(V)
	ret1, _ := ret[1].(bool)
	return ret0, ret1
}

// Get sets up expectations for calls to Get.
// Calling this method multiple times allows expecting multiple calls to Get with a variety of parameters.
//
// Inputs:
//
//	key K
//
// Outputs:
//
//	value V
//	ok bool
func (mr *MockInfallibleStoreMockRecorder[K, V]) Get(_key interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	inputs := []interface{}{_key}
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Get", reflect.TypeOf((*MockInfallibleStore[K, V])(nil).Get), inputs...)
}

// Put mocks Put on InfallibleStore.
func (m *MockInfallibleStore[K, V]) Put(_key K, _value V) {
	m.ctrl.T.Helper()
	inputs := []interface{}{_key, _value}
	ret := m.ctrl.Call(m, "Put", inputs...)
	var _ = ret // Unused, since there are no returns
	return
}

// Put sets up expectations for calls to Put.
// Calling this method multiple times allows expecting multiple calls to Put with a variety of parameters.
//
// Inputs:
//
//	key K
//	value V
//
// Outputs:
//
//	none
func (mr *MockInfallibleStoreMockRecorder[K, V]) Put(_key interface{}, _value interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	inputs := []interface{}{_key, _value}
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Put", reflect.TypeOf((*MockInfallibleStore[K, V])(nil).Put), inputs...)
}

// MockIterator is a mock of the Iterator interface in gitlab.com/Fryuni/geny.
type MockIterator[V any] struct {
	ctrl     *gomock.Controller
	recorder *MockIteratorMockRecorder[V]
}

// MockIteratorMockRecorder is the mock recorder for MockIterator.
type MockIteratorMockRecorder[V any] struct {
	mock *MockIterator[V]
}

// NewMockIterator creates a new mock instance.
func NewMockIterator[V any](ctrl *gomock.Controller) *MockIterator[V] {
	mock := &MockIterator[V]{ctrl: ctrl}
	mock.recorder = &MockIteratorMockRecorder[V]{mock}
	return mock
}

// NEW creates a MockIterator. This method is used internally by ensure.
func (*MockIterator[V]) NEW(ctrl *gomock.Controller) *MockIterator[V] {
	return NewMockIterator[V](ctrl)
}

// EXPECT returns a struct that allows setting up expectations.
func (m *MockIterator[V]) EXPECT() *MockIteratorMockRecorder[V] {
	return m.recorder
}

// Next mocks Next on Iterator.
func (m *MockIterator[V]) Next() (_value V, _err error) {
	m.ctrl.T.Helper()
	inputs := []interface{}{}
	ret := m.ctrl.Call(m, "Next", inputs...)
	ret0, _ := ret[0].(V)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Next sets up expectations for calls to Next.
// Calling this method multiple times allows expecting multiple calls to Next with a variety of parameters.
//
// Inputs:
//
//	none
//
// Outputs:
//
//	value V
//	err error
func (mr *MockIteratorMockRecorder[V]) Next() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	inputs := []interface{}{}
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Next", reflect.TypeOf((*MockIterator[V])(nil).Next), inputs...)
}

// MockContextIterator is a mock of the ContextIterator interface in gitlab.com/Fryuni/geny.
type MockContextIterator[V any] struct {
	ctrl     *gomock.Controller
	recorder *MockContextIteratorMockRecorder[V]
}

// MockContextIteratorMockRecorder is the mock recorder for MockContextIterator.
type MockContextIteratorMockRecorder[V any] struct {
	mock *MockContextIterator[V]
}

// NewMockContextIterator creates a new mock instance.
func NewMockContextIterator[V any](ctrl *gomock.Controller) *MockContextIterator[V] {
	mock := &MockContextIterator[V]{ctrl: ctrl}
	mock.recorder = &MockContextIteratorMockRecorder[V]{mock}
	return mock
}

// NEW creates a MockContextIterator. This method is used internally by ensure.
func (*MockContextIterator[V]) NEW(ctrl *gomock.Controller) *MockContextIterator[V] {
	return NewMockContextIterator[V](ctrl)
}

// EXPECT returns a struct that allows setting up expectations.
func (m *MockContextIterator[V]) EXPECT() *MockContextIteratorMockRecorder[V] {
	return m.recorder
}

// Next mocks Next on ContextIterator.
func (m *MockContextIterator[V]) Next(_ctx context.Context) (_value V, _err error) {
	m.ctrl.T.Helper()
	inputs := []interface{}{_ctx}
	ret := m.ctrl.Call(m, "Next", inputs...)
	ret0, _ := ret[0].(V)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Next sets up expectations for calls to Next.
// Calling this method multiple times allows expecting multiple calls to Next with a variety of parameters.
//
// Inputs:
//
//	ctx context.Context
//
// Outputs:
//
//	value V
//	err error
func (mr *MockContextIteratorMockRecorder[V]) Next(_ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	inputs := []interface{}{_ctx}
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Next", reflect.TypeOf((*MockContextIterator[V])(nil).Next), inputs...)
}
