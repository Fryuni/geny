package geny

import (
	"context"
	"errors"
)

// ErrNoItem is returned by Store.Get and ContextStore.Get when the key is not found.
var ErrNoItem = errors.New("no item")

type (
	// InfallibleStore is a store that does not return errors.
	//
	// The API is conceptually the same as map[K]V, but can be implemented by a wider range of data structures.
	// Just like the native map, the operations may panic, but not fail.
	InfallibleStore[K any, V any] interface {
		// Get returns the value associated with the given key.
		//
		// If the key is not found, `ok` is false.
		Get(key K) (value V, ok bool)

		// Put associates the given value with the given key.
		Put(key K, value V)

		// Delete removes the value associated with the given key.
		Delete(key K)
	}

	// Store is an abstraction over a key-value store.
	//
	// The API is similar to map[K]V, but each operation may return an error.
	// To convert a Store to an InfallibleStore, which has the same API as map[K]V, use the store.Must function.
	Store[K any, V any] interface {
		// Get returns the value associated with the given key.
		//
		// If the key is not found, `err` is ErrNoItem and `value` is the zero value of type V.
		Get(key K) (value V, err error)

		// Put associates the given value with the given key.
		Put(key K, value V) error

		// Delete removes the value associated with the given key.
		Delete(key K) error
	}

	// ContextStore is a Store that supports cancellation through a context.Context.
	ContextStore[K any, V any] interface {
		// Get returns the value associated with the given key.
		//
		// If the key is not found, `err` is ErrNoItem and `value` is the zero value of type V.
		Get(ctx context.Context, key K) (value V, err error)

		// Put associates the given value with the given key.
		Put(ctx context.Context, key K, value V) error

		// Delete removes the value associated with the given key.
		Delete(ctx context.Context, key K) error
	}

	// IterableStore is a Store that allows iterating over all its items.
	IterableStore[K any, V any] interface {
		IterItems() Iterator[KV[K, V]]
	}

	// IterableContextStore is a ContextStore that allows iterating over all its items.
	IterableContextStore[K any, V any] interface {
		IterItems(ctx context.Context) Iterator[KV[K, V]]
	}

	// KV is a key-value pair.
	KV[K any, V any] struct {
		Key   K
		Value V
	}
)

type InMemoryMapStore[K comparable, V any] map[K]V

func (i InMemoryMapStore[K, V]) Get(key K) (value V, ok bool) {
	value, ok = i[key]
	return
}

func (i InMemoryMapStore[K, V]) Put(key K, value V) {
	i[key] = value
}

func (i InMemoryMapStore[K, V]) Delete(key K) {
	delete(i, key)
}
